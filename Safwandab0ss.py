import RPi.GPIO as GPIO
import time

##whee1=[2,3]
##whee2=[14,15]
wheels=[2,3,14,15]

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(wheels,GPIO.OUT)

def getch():
    import sys, tty, termios
    fd=sys.stdin.fileno()
    old_settings=termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch=sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd,termios.TCSADRAIN,old_settings)
    return ch

def left():
    GPIO.output(14,GPIO.LOW)
    GPIO.output(15,GPIO.HIGH)
    GPIO.output(2,GPIO.LOW)
    GPIO.output(3,GPIO.HIGH)
    ##time.sleep(5)
##left()

def right():
    GPIO.output(3,GPIO.LOW)
    GPIO.output(2,GPIO.HIGH)
    GPIO.output(14,GPIO.HIGH)
    GPIO.output(15,GPIO.LOW)
    ##time.sleep(5)
##right()

def backward():
    GPIO.output(3,GPIO.HIGH)
    GPIO.output(14,GPIO.HIGH)
    GPIO.output(2,GPIO.LOW)
    GPIO.output(15,GPIO.LOW)
    ##time.sleep(5)
##backward()

def forward():
    GPIO.output(2,GPIO.HIGH)
    GPIO.output(15, GPIO.HIGH)
    GPIO.output(3,GPIO.LOW)
    GPIO.output(14,GPIO.LOW)
    ##time.sleep(5)
##forward()

def stop():
    GPIO.output(2,GPIO.LOW)
    GPIO.output(15,GPIO.LOW)
    GPIO.output(3,GPIO.LOW)
    GPIO.output(14,GPIO.LOW)
    ##time.sleep(5)
##stop()
    
while True:

    g=getch()
    if g=='a':
        print('left')
        left()
    elif g=='d':
        print('right')
        right()
    elif g=='w':
        print('forward')
        forward()
    elif g=='s':
        print('backward')
        backward()
    elif g=='q':
        stop()
        print('stop')
    elif g=='p':
        GPIO.cleanup()

##GPIO.setup(whee1,GPIO.OUT)
##GPIO.setup(whee2,GPIO.OUT)
##GPIO.output(2,GPIO.HIGH)
##GPIO.output(14,GPIO.HIGH)
##GPIO.output(3,GPIO.LOW)
##GPIO.output(15,GPIO.LOW)
##time.sleep(5)

##GPIO.cleanup()

##import random
##bob=['Manchester United', 'Chelsea']
##random=random.choice (bob)
##print (random)
##
###Machester United 35
###Chelsea 28
